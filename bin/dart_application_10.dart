import 'dart:io';

int price = 0;
void main() {
  Menu order = Menu();
  Type ordertype = Type();
  Topping orderontop = Topping();
  Lodfah lnf = Lodfah();
  Payment pay = Payment();
  var type;
  int drinktype;
  int menuprice;
  //int toppingtype;
  print("--- Welcome to taobin !!! ---");
  print(
      "--- Please choose your category of drinks ( โปรดเลือกหมวดหมู่เครื่องดื่มของคุณ ) ---\n1 : Coffee ( กาแฟ )\n2 : Cha ( ชา )\n3 : Milk ( นม )\n4 : Soda&other ( โซดาและอื่นๆ )");
  var inputcategory = stdin.readLineSync();
  if (inputcategory == "1") {
    //order.coffee();
    menuprice = int.parse(order.coffee());
    print(
        "--- Please choose your drink type ( โปรดเลือกชนิดของเครื่องดื่มของคุณ ) ---\n1 : Hot ( ร้อน ) \n2 : Iced ( เย็น ) \n3 : Frappe ( ปั่น )");
    type = stdin.readLineSync();
    if (type == "1") {
      drinktype = int.parse(ordertype.hot());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    } else if (type == "2") {
      drinktype = int.parse(ordertype.iced());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    } else if (type == "3") {
      drinktype = int.parse(ordertype.frappe());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    }
  }
  if (inputcategory == "2") {
    //order.cha();
    menuprice = int.parse(order.cha());
    print(
        "--- Please choose your drink type ( โปรดเลือกชนิดของเครื่องดื่มของคุณ ) ---\n1 : Hot ( ร้อน ) \n2 : Iced (เย็น) \n3 : Frappe (ปั่น)");
    type = stdin.readLineSync();
    if (type == "1") {
      drinktype = int.parse(ordertype.hot());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    } else if (type == "2") {
      drinktype = int.parse(ordertype.iced());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    } else if (type == "3") {
      drinktype = int.parse(ordertype.frappe());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    }
  }
  if (inputcategory == "3") {
    //order.milk();
    menuprice = int.parse(order.milk());
    print(
        "--- Please choose your drink type ( โปรดเลือกชนิดของเครื่องดื่มของคุณ ) ---\n1 : Hot ( ร้อน ) \n2 : Iced ( เย็น ) \n3 : Frappe ( ปั่น )");
    type = stdin.readLineSync();
    if (type == "1") {
      drinktype = int.parse(ordertype.hot());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    } else if (type == "2") {
      drinktype = int.parse(ordertype.iced());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    } else if (type == "3") {
      drinktype = int.parse(ordertype.frappe());
      price = drinktype + menuprice;
      print(
          "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
    }
  }
  if (inputcategory == "4") {
    //order.other();
    menuprice = int.parse(order.other());
    print(
        "*** Price of your drink ( ราคาเครื่องดื่มของคุณ ) : $menuprice บาท ***");
    price = price + menuprice;
  }

  if (inputcategory == "1" || inputcategory == "2" || inputcategory == "3") {
    print(
        "---Please choose Topping you want ( โปรดเลือกทอปปิ้งที่ต้องการ ) --- \n0 : No ( ไม่เพิ่ม ) \n1 : Ovaltine ( ผงโอวัลติน ) \n2 : Oreo ( ผงโอริโอ ) \n3 : Browny ( บราวนี่ )");
    var topp = stdin.readLineSync();
    if (topp == "0") {
      price = price + 0;
    } else if (topp == "1") {
      price = price + orderontop.oreo();
    } else if (topp == "2") {
      price = price + orderontop.ovaltine();
    } else if (topp == "3") {
      price = price + orderontop.browny();
    }
    print(
        "*** Now price of your drink ( ราคาเครื่องดื่มของคุณตอนนี้ ) : $price บาท ***");
  }

  if (inputcategory == "1" ||
      inputcategory == "2" ||
      inputcategory == "3" ||
      inputcategory == "4") {
    print(
        "--- Please choose your drink equipment ( โปรเลือกอุปกรณ์ในการดื่ม ) ---\n0 : No ( ไม่รับ ) \n1 : Lid ( ฝา ) \n2 : Bulb ( หลอด ) \n3 : Lid & Bulb ( ฝาและหลอด )");
    var eqm = stdin.readLineSync();
    if (eqm == "0") {
      lnf.noequipment();
    } else if (eqm == "1") {
      lnf.fha();
    } else if (eqm == "2") {
      lnf.lod();
    } else if (eqm == "3") {
      lnf.fha();
      lnf.lod();
    }
  }
  print(
      "--- Please choose your payment ( โปรดเลือกวิธีการชำระเงิน ) --- \n1 : cash ( เงินสด ) \n2 : Promptpay \n3 : truemoney wallet");
  var typepayment = stdin.readLineSync();
  if (typepayment == "1") {
    int total = 0;
    int count = 0;
    print("price : $price baht");
    print(
        "--- Please input your cash ( โปรดใส่เงินตามจำนวนราคาเครื่องดื่มของคุณ ) ---");
    var inputcash = stdin.readLineSync();
    int changecash = int.parse(inputcash!);
    count = count + changecash;
    while (count < price) {
      print("จำนวนเงินที่ใส่มาแล้ว $count บาท");
      var inputcash = stdin.readLineSync();
      int changecash = int.parse(inputcash!);
      count = count + changecash;
    }

    if (count > price) {
      total = count - price;
      print("..........");
      print(".....");
      print("...");
      print("*-*- โปรดรับเงินทอนเป็นจำนวนเงิน $total บาท -*-*");
      print("---*** Thank you ( ขอบคุณที่ใช้บริการเต่าบิน ) ***---");
    } else if (count == price || changecash == price) {
      print("..........");
      print(".....");
      print("...");
      print("---*** Thank you ( ขอบคุณที่ใช้บริการเต่าบิน ) ***---");
    }
  } else if (typepayment == "2") {
    print("price : $price baht");
    pay.promptpay();
  } else if (typepayment == "3") {
    print("price : $price baht");
    pay.truemoneywallet();
  }
}

abstract class Category {
  void coffee();
  void cha();
  void milk();
  void other();
}

class Menu extends Category {
  @override
  String coffee() {
    var menudrink;
    print("--- Category : Coffee ( หมวดหมู่ : กาแฟ ) ---");
    print(
        "--- List of Menu ( รายชื่อเมนู ) ---\n1 : Mocha ( มอคค่า )\n2 : Black coffee ( กาแฟดำ )\n3 : Latte ( ลาเต้ )");
    var drink = stdin.readLineSync();
    if (drink == "1") {
      print("--- Your menu is : Mocha ( เมนูที่คุณเลือก : มอคค่า ) ---");
      menudrink = "35";
    } else if (drink == "2") {
      print("--- Your menu is : Black coffee ( เมนูที่คุณเลือก : กาแฟดำ ) ---");
      menudrink = "30";
    } else if (drink == "3") {
      print("--- Your menu is : Latte ( เมนูที่คุณเลือก : ลาเต้ ) ---");
      menudrink = "35";
    }
    return menudrink;
  }

  @override
  String cha() {
    var menudrink;
    print("--- Category : Cha ( หมวดหมู่ : ชา ) ---");
    print(
        "--- List of Menu ( รายชื่อเมนู ) ---\n1 : Green tea ( ชาเขียว )\n2 : Black tea ( ชาดำ )\n3 : Milk tea ( ชานม )");
    var drink = stdin.readLineSync();
    if (drink == "1") {
      print("--- Your menu is : Green tea ( เมนูที่คุณเลือก : ชาเขียว ) ---");
      menudrink = "35";
    } else if (drink == "2") {
      print("--- Your menu is : Black tea ( เมนูที่คุณเลือก : ชาดำ ) ---");
      menudrink = "30";
    } else if (drink == "3") {
      print("--- Your menu is : Milk tea ( เมนูที่คุณเลือก : ชานม ) ---");
      menudrink = "35";
    }
    return menudrink;
  }

  @override
  String milk() {
    var menudrink;
    print("--- Category : milk ( หมวดหมู่ : นม ) ---");
    print(
        "--- List of Menu ( รายชื่อเมนู ) ---\n1 : Coacoa ( โกโก้ )\n2 : Ovaltine ( โอวัลติน )\n3 : Milk ( นม )");
    var drink = stdin.readLineSync();
    if (drink == "35") {
      print("--- Your menu is : Coacoa ( เมนูที่คุณเลือก : โกโก้ ) ---");
      menudrink = "โกโก้";
    } else if (drink == "2") {
      print("--- Your menu is : Ovaltine ( เมนูที่คุณเลือก : โอวัลติน ) ---");
      menudrink = "35";
    } else if (drink == "3") {
      print("--- Your menu is : Milk ( เมนูที่คุณเลือก : นม ) ---");
      menudrink = "30";
    }
    return menudrink;
  }

  @override
  String other() {
    var menudrink;
    print("--- Category : Soda&other ( หมวดหมู่ : โซดาและอื่นๆ ) ---");
    print(
        "--- List of Menu ( รายชื่อเมนู ) ---\n1 : Strawberry soda(iced) ( สตร์อวเบอรี่โซดา(เย็น) )\n2 : Whey protein(hot) ( เวย์โกโก้(ร้อน) )\n3 : Taobin(bottle) ( เต่าบิน(ขวด) )");
    var drink = stdin.readLineSync();
    if (drink == "1") {
      print(
          "--- Your menu is : Strawberry soda(iced) ( เมนูที่คุณเลือก : สตร์อวเบอรี่โซดา(เย็น) ) ---");
      menudrink = "35";
    } else if (drink == "2") {
      print(
          "--- Your menu is : Whey protein(hot) ( เมนูที่คุณเลือก : เวย์โกโก้(ร้อน) ) ---");
      menudrink = "35";
    } else if (drink == "3") {
      print(
          "--- Your menu is : Taobin(bottle) ( เมนูที่คุณเลือก : เต่าบิน(ขวด) ) ---");
      menudrink = "15";
    }
    return menudrink;
  }
}

abstract class Menutype {
  void hot();
  void iced();
  void frappe();
}

class Type extends Menutype {
  @override
  String hot() {
    String hp = "0";
    return hp;
  }

  @override
  String iced() {
    String ip = "5";
    return ip;
  }

  @override
  String frappe() {
    String fp = "10";
    return fp;
  }
}

abstract class Menutopping {
  void ovaltine();
  void oreo();
  void browny();
}

class Topping extends Menutopping {
  @override
  int ovaltine() {
    int oval = 5;
    return oval;
  }

  @override
  int oreo() {
    int ore = 5;
    return ore;
  }

  @override
  int browny() {
    int bny = 5;
    return bny;
  }
}

abstract class LodAndFha {
  void noequipment();
  void lod();
  void fha();
}

class Lodfah extends LodAndFha {
  @override
  void fha() {
    print("--- Please pick up Lid ( โปรดรับฝา ) ---");
  }

  @override
  void lod() {
    print("--- Please pick up Bulb ( โปรดรับหลอด ) ---");
  }

  @override
  void noequipment() {
    print("Thank you !!!");
  }
}

abstract class Checkbill {
  void promptpay();
  void truemoneywallet();
}

class Payment extends Checkbill {
  @override
  void promptpay() {
    print("--- QRCODE PROMPTPAY ---");
    print("..........");
    print(".....");
    print("...");
    print("---*** Thank you ( ขอบคุณที่ใช้บริการเต่าบิน ) ***---");
  }

  @override
  void truemoneywallet() {
    print("--- QRCODE TRUEMONEY WALLET ---");
    print("..........");
    print(".....");
    print("...");
    print("---*** Thank you ( ขอบคุณที่ใช้บริการเต่าบิน ) ***---");
  }
}
